<?php

require 'OrderDispatch.php';

$couriers = [
    'UPS' => 1,
    'RoyalMail' => 2
];

// Create new OrderDispatch instance which will be used to harvest / send or consignments
$OrderDispatch = new OrderDispatch;

// Create a new 'batch'. Additional meta data could be passed to this down the line
$batch = $OrderDispatch->createBatch();

// Add an order to the batch. This method expects the order ID, and the courier ID
$batch->addConsignment(83744, $couriers['UPS']);
$batch->addConsignment(99384, $couriers['RoyalMail']);

// Send consignments to all relevant couriers
$consignments = $batch->sendBatch();

print_r($consignments);