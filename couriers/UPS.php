<?php

class UPS implements CourierInterface
{
    public function __construct()
    {
    }

    /**
     * Generate consignment number
     * @return string
     */
    public function generateConsignmentNumber()
    {
        return 'UPS-8374';
    }

    /**
     * Send consignments to courier for processing
     * @return void
     */
    public function sendConsignments(array $consignments)
    {
        // Send via preferred method
    }
}

?>