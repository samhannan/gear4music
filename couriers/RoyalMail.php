<?php

class RoyalMail implements CourierInterface
{
    public function __construct()
    {
    }

    /**
     * Generate consignment number
     * @return string
     */
    public function generateConsignmentNumber()
    {
        return 'RM-8374';
    }

    /**
     * Send consignments to courier for processing
     * @return void
     */
    public function sendConsignments(array $consignments)
    {
        // Send via preferred method
    }
}

?>