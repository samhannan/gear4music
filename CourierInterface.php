<?php

interface CourierInterface
{
    /**
     * Generate a consignment number based on the spec provided by the implementing courier
     * @return void
     */
    public function generateConsignmentNumber();

    /**
     * Send consignments to the courier based on the spec provided by the implementing courier
     * @param array $consignments
     * @return void
     */
    public function sendConsignments(array $consignments);
}

?>