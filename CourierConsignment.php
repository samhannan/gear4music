<?php

require 'CourierInterface.php';
require 'couriers/UPS.php';
require 'couriers/RoyalMail.php';

class CourierConsignment
{
    const COURIER_UPS = 1,
          COURIER_ROYAL_MAIL = 2;

    /**
     * The courier ID this instance is accociated with
     * @var int
     */
    private $courierID;

    /**
     * The courier class to be used for Courier specific functionality
     * @var object
     */
    private $CourierClass;

    /**
     * This class expects a valid courier ID to be provided.
     * If an invalid ID is provided, and exception will be thown.
     * 
     * @param integer $courierID
     */
    public function __construct(int $courierID)
    {
        $this->courierID = $courierID;

        $CourierClass = $this->getCourierClass();

        if(is_null($CourierClass)) {
            throw new \Exception('This courier does not appear to exist');
        }

        $this->CourierClass = $CourierClass;
    }

    /**
     * Locate the class representing the active courier.
     * 
     * Each courier class contains courier specific logic enfoced by a shared interface
     * 
     * @return object
     */
    private function getCourierClass()
    {
        switch($this->courierID) {
            case self::COURIER_UPS:
                return new UPS;
                break;
            case self::COURIER_ROYAL_MAIL:
                return new RoyalMail;
                break;
                
            // and so on...
        }

        return null;
    }

    /**
     * Generate a consignment number
     * 
     * @return string
     */
    public function generateConsignmentNumber()
    {
        return $this->CourierClass->generateConsignmentNumber();
    }

    /**
     * Send consignments to the appropriate courier based on the method within the 
     * associate Courier class
     * 
     * @return void
     */
    public function sendConsignmentsToCourier(array $consignments)
    {
        return $this->CourierClass->sendConsignments($consignments);
    }

    
}

?>