# Gear4Music Technical Exercise

### Summary

This application is a demonstration of a potenial implementation of a practical challenge faced by Gear4Music. My implementation is designed to be as modular and scaleable as possible given the time frame.

### How to use

The core class contained with the application can be found in `OrderDispatch.php`.

A typical front-end implementation of this class would look something like this:

```
$OrderDispatch = new OrderDispatch;

$consignments = $OrderDispatch->createBatch()
    ->addConsignment(83744, $couriers['UPS'])
    ->addConsignment(99384, $couriers['RoyalMail'])
    ->sendBatch();   
```

As you can see from the above, the class contains 3 public methods:

`createBatch()` - This creates a new batch; A Batch ID can be passed in to the class construct if updating an existing batch.

`addConsignment($orderID, $courierID)` - This adds an order to the class for consignment. The first argument being the order ID, and the second being the courier ID. Multiple method requests can be chained.

`sendBatch()` - This method sends the consignments to the appropriate couriers and flags the batch as 'complete', or 'sent'.

This example can be found in `index.php`.
