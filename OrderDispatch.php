<?php

require 'CourierConsignment.php';

class OrderDispatch
{   
    /**
     * The batch we are currently creating / updating
     * @var int
     */
    private $batchID;

    /**
     * Class construct
     * @param integer $batchID - If we are updating an existing batch, the batch ID can be provided
     */
    public function __construct(int $batchID = null)
    {
        if(!is_null($batchID)) {
            $this->batchID = $batchID;
        }
    }

    /**
     * Create a new batch
     * 
     * @return self
     */
    public function createBatch()
    {
        /*
         * At this point, I would immediately save a new batch to persistant storage.
         * However, for the purposes of this excercise lets assume we have created a DB record
         * which has provided us with a batch ID.
         */
        $this->batchID = 3847;

        return $this;
    }

    /**
     * Add new consignment to batch
     
     * @param integer $orderID - The order this consignment relates to
     * @param integer $courierID
     * @return self
     */
    public function addConsignment(int $orderID, int $courierID)
    {
        $consignmentNumber = $this->generateConsignmentNumber($courierID);

        /*
         * I would now save the new consignment to persistant storage.
         * Lets assume this has been done, and we are provided with an array representing
         * a DB table record as follows:
         */
        
        /* $consignment = [
            'id' => 1234,
            'consignment_number' => 'AB-1234',
            'courier_id' => 1
        ]; */

        return $this;
    }

    /**
     * Send a batch of consignments to each courier in the collection
     * 
     * @return void
     */
    public function sendBatch()
    {
        $consignments = $this->getConsignments();

        $groupedConsignments = $this->groupConsignmentsByCourier($consignments);
        
        foreach($groupedConsignments as $consignments) {
            $CourierConsignment = new CourierConsignment($consignments['courier_id']);
            $CourierConsignment->sendConsignmentsToCourier($consignments['consignments']);
        }

        // Flag batch as 'sent' and store in DB...
        
        return $this->getConsignments();
    }

    /**
     * Retrieve all consigments attached to this batch
     * For the purposes of this exercise, a hard coded array of data is returned
     * 
     * @return array
     */
    public function getConsignments()
    {
        return [
            [
                'id' => 1234,
                'consignment_number' => 'AB-1234',
                'courier_id' => 1
            ],
            [
                'id' => 5678,
                'consignment_number' => '938FGP345',
                'courier_id' => 2
            ],
        ];
    }

    /**
     * Group all consigments by courier. This makes it easier to post a 'batch' of 
     * consignments to their respective courier.
     * 
     * @param array $consignments
     * @return void
     */
    private function groupConsignmentsByCourier(array $consignments)
    {
        return [
            [
                'courier_id' => 1,
                'consignments' => [
                    [
                        'id' => 1234,
                        'consignment_number' => 'AB-1234',
                    ]
                ]
            ],
            [
                'courier_id' => 2,
                'consignments' => [
                    [
                        'id' => 6574,
                        'consignment_number' => 'CF34343',
                    ]
                ]
            ]
        ];
    }

    /**
     * Generate a courier consignment number based on a specified courier
     * 
     * @param integer $courierID - The courier ID to generate a number for 
     * @return string
     */
    private function generateConsignmentNumber(int $courierID)
    {     
        $CourierConsignment = new CourierConsignment($courierID);

        return $CourierConsignment->generateConsignmentNumber($courierID);
    }
}

?>